package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var firstVariable:Double = 0.0
    private var secondVariable:Double = 0.0
    private var operaion = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        deleteButton.setOnLongClickListener {
            resultTextView.text=""
            return@setOnLongClickListener true
        }

        dotButton.setOnClickListener {
            if(resultTextView.text.isNotEmpty() && "." !in resultTextView.text.toString()){
            resultTextView.text = resultTextView.text.toString() + "."
            }
        }
    }


    fun assembly(view: View){
        var value = resultTextView.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operaion = "+"
            resultTextView.text = ""
        }

    }

    fun subtraction(view: View){
        var value = resultTextView.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operaion = "-"
            resultTextView.text = ""
        }

    }

    fun multiplication(view: View){
        var value = resultTextView.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operaion = "X"
            resultTextView.text = ""
        }

    }

    fun equal(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && operaion.isNotEmpty()) {
            secondVariable = value.toDouble()
            var result:Double = 0.0
            if (operaion == ":") {
                result = firstVariable / secondVariable
            }
            if(operaion == "X") {
                result = firstVariable * secondVariable
            }
            if(operaion == "+"){
                result = firstVariable + secondVariable
            }
            if(operaion == "-"){
                result = firstVariable - secondVariable
            }

            resultTextView.text = result.toString()

            operaion = ""
            firstVariable = 0.0
            secondVariable = 0.0
        }
    }

    fun delete(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty())
        resultTextView.text = value.substring(0, value.length - 1)
    }
    fun divide(view: View){
        var value = resultTextView.text.toString()
        if(value.isNotEmpty()) {
            firstVariable = value.toDouble()
            operaion = ":"
            resultTextView.text = ""
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text=resultTextView.text.toString() + button.text.toString()
    }
}